//
//  CountryModel.swift
//  Countries
//
//  Created by jean carlos on 10/09/18.
//  Copyright © 2018 ramos. All rights reserved.
//

import UIKit
import SwiftyJSON

class CountryModel: NSObject {
    
    fileprivate static let kStoredCountryModelKey:String = "Dependency"
    
    //Mark Properties
    var code: Int = 0
    var name: String=""
    
    //MARK Constructor
    override init() {
        super.init()
    }
    
    func copyFromJSON(_ json: JSON){
        guard let body = json as JSON? else {
            return
        }
        self.code = body["code"].intValue
        self.name = body["name"].stringValue
    }
    
    class func countries(fromJSONArray array: [JSON]?) -> [CountryModel]{
        var countries : [CountryModel] = []
        if let data = array{
            for item in data{
                countries.append(CountryModel.country(fromJSON: item)!)
            }
        }
        return countries
    }
    
    class func country(fromJSON json: JSON?) -> CountryModel?{
        if let data = json {
            let country = CountryModel()
            country.code = data["code"].intValue
            country.name = data["name"].stringValue
            
            return country
        }
        return nil
    }
    
    func archiveCountry(){
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(data, forKey: CountryModel.kStoredCountryModelKey)
        UserDefaults.standard.synchronize()
    }
    
    func unarchiveCountry(){
        UserDefaults.standard.removeObject(forKey: CountryModel.kStoredCountryModelKey)
        UserDefaults.standard.synchronize()
    }
    
    class func retriveCountry() -> CountryModel? {
        guard let data = UserDefaults.standard.object(forKey: CountryModel.kStoredCountryModelKey) as? Data else {
            return nil
        }
        return NSKeyedUnarchiver.unarchiveObject(with: data) as? CountryModel
    }

}
