//
//  UIViewController+SVProgressHUD.swift
//  Countries
//
//  Created by jean carlos on 10/09/18.
//  Copyright © 2018 ramos. All rights reserved.
//

import UIKit
import SVProgressHUD

extension UIViewController {
    
    func showActivityIndicator() {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setForegroundColor(UIColor(red:0/255, green:122/255, blue:255/255, alpha:1.0))
        SVProgressHUD.show(withStatus: "Cargando...")
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async(execute: {
            SVProgressHUD.dismiss()
        })
    }
    
}
