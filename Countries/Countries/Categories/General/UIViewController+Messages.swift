//
//  UIViewController+Messages.swift
//  Countries
//
//  Created by jean carlos on 10/09/18.
//  Copyright © 2018 ramos. All rights reserved.
//


import Foundation
import SwiftMessages

extension UIViewController {
    
    func showErrorMessage(withTitle title:String) -> Void {
        SwiftMessages.hideAll()
        let view: MessageView = try! SwiftMessages.viewFromNib(named: "ErrorMessageView")
        view.bodyLabel?.text = title
        SwiftMessages.show(view: view)
    }
    
    func showSuccessMessage(withTitle title:String) -> Void {
        SwiftMessages.hideAll()
        let view: MessageView = try! SwiftMessages.viewFromNib(named: "SuccessMessageView")
        view.bodyLabel?.text = title
        SwiftMessages.show(view: view)
    }
    
    func showErrorMessage(withTitle title:String, andFirstResponder textField:UITextField) -> Void {
        self.showErrorMessage(withTitle: title)
        textField.becomeFirstResponder()
    }
    
    func showSuccessMessage(withTitle title:String, andFirstResponder textField:UITextField) -> Void {
        self.showSuccessMessage(withTitle: title)
        textField.becomeFirstResponder()
    }
}

