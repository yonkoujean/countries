//
//  CountriesManager.swift
//  Countries
//
//  Created by jean carlos on 10/09/18.
//  Copyright © 2018 ramos. All rights reserved.
//

import UIKit

class CountriesManager: NSObject {

    static let sharedManager = CountriesManager()
    
    var countries :[CountryModel] = []
    
    
    override init() {
        super.init()
        
        initCountries()
    }
    
    //Indicator 1
    func loadCountries(success: @escaping(_ countries: [CountryModel]) -> Void, failure: @escaping(_ error:NSError) -> Void){
        
        if countries.count > 0 {
            success(countries)
        }else{
            //No hay elementos
        }
        
    }
    
    func addNewCountry(_ country : String){
        let currentCountry = CountryModel()
        currentCountry.name = country
        currentCountry.code = countries.count
        countries.append(currentCountry)
    }
    
    func initCountries(){
        //Inicializamos con algnunos países
        let country1 = CountryModel()
        country1.code = 0
        country1.name = "Argentina"
        countries.append(country1)
        
        let country2 = CountryModel()
        country2.code = 1
        country2.name = "Brasil"
        countries.append(country2)
        
        let country3 = CountryModel()
        country3.code = 2
        country3.name = "Colombia"
        countries.append(country3)
        
        let country4 = CountryModel()
        country4.code = 3
        country4.name = "Perú"
        countries.append(country4)
    }
}
