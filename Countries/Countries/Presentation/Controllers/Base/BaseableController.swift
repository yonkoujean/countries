//
//  BaseableController.swift
//  Countries
//
//  Created by jean carlos on 10/09/18.
//  Copyright © 2018 ramos. All rights reserved.
//

import Foundation
import UIKit

protocol BaseableController {
    func baseSetup() -> Void
}

extension BaseableController where Self: UIViewController{
    func baseSetup(){
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

