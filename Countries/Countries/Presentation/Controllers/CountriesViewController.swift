//
//  CountriesViewController.swift
//  Countries
//
//  Created by jean carlos on 10/09/18.
//  Copyright © 2018 ramos. All rights reserved.
//

import UIKit

class CountriesViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var btnAddCountry: UIButton!
    @IBOutlet weak var tvCountries: UITableView!
    
    var listCountries : [CountryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tvCountries.dataSource = self
        self.tvCountries.delegate = self
        
        loadCountries()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCountries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvCountries.dequeueReusableCell(withIdentifier: "Country Cell", for: indexPath)
        let country : CountryModel
        country = listCountries[indexPath.row]
        cell.textLabel!.text = country.name
        
        return cell
    }
    
    func loadCountries() {
        let manager = CountriesManager.sharedManager
        self.showActivityIndicator()
        
        manager.loadCountries(success: {
            countries in
            self.hideActivityIndicator()
            self.listCountries = countries
            self.tvCountries.reloadData()
            
        }, failure: {
            error in
            self.requestDidFinishWithError(error)
        })
    }
    
    func cleanFields(){
        self.tfCountry.text = ""
    }
    
    @IBAction func addCountry(_ sender: Any) {
        if tfCountry.text != ""{
            
            for item in self.listCountries{
                if item.name == tfCountry.text { //Nombre repetido
                    self.showErrorMessage(withTitle: "\(self.tfCountry.text!) ya existe")
                    return
                }
            }
            let manager = CountriesManager.sharedManager
            manager.addNewCountry(tfCountry.text!)
            cleanFields()
            loadCountries()
            self.showSuccessMessage(withTitle: "Ingresado exitosamente")
        }else{
            self.showErrorMessage(withTitle: "Ingrese país válido")
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
